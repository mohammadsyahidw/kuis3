from .base import FunctionalTest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class NewVisitorTest(FunctionalTest):
	
    def test_can_start_a_list_and_retrieve_it_later(self):  #4
	        
	# Edith has heard about a cool new online to-do app. She goes
        # to check out its homepage
        self.browser.get(self.server_url)
	
        # She notices the page title and header mention to-do lists
        self.assertIn('To-Do', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('welcome to', header_text)

        # She is invited to enter a to-do item straight away
        
        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(
                inputbox.get_attribute('placeholder'),
                'Enter a to-do item'
        )
        """
        status = self.browser.find_element_by_id('status')
        self.assertEqual(
                status.text,
                'yey waktunya berlibur'
        )
        inputbox.send_keys('Halo')
        inputbox.send_keys(Keys.ENTER)
        self.check_for_row_in_list_table('1: Halo')
        status = self.browser.find_element_by_id('status')
        self.assertEqual(
                status.text,
                'sibuk tapi santai'
        )

        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('Apa')
        inputbox.send_keys(Keys.ENTER)
        self.check_for_row_in_list_table('1: Halo')
        self.check_for_row_in_list_table('2: Apa')
        status = self.browser.find_element_by_id('status')
        self.assertEqual(
                status.text,
                'sibuk tapi santai'
        )
        
        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('Kabar')
        inputbox.send_keys(Keys.ENTER)
        self.check_for_row_in_list_table('1: Halo')
        self.check_for_row_in_list_table('2: Apa')
        self.check_for_row_in_list_table('3: Kabar')
        status = self.browser.find_element_by_id('status')
        self.assertEqual(
                status.text,
                'sibuk tapi santai'
        )
        
        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('Pagi')
        inputbox.send_keys(Keys.ENTER)
        self.check_for_row_in_list_table('1: Halo')
        self.check_for_row_in_list_table('2: Apa')
        self.check_for_row_in_list_table('3: Kabar')
        self.check_for_row_in_list_table('4: Pagi')
        status = self.browser.find_element_by_id('status')
        self.assertEqual(
                status.text,
                'sibuk tapi santai'
        )
        
        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('Ini')
        inputbox.send_keys(Keys.ENTER)
        self.check_for_row_in_list_table('1: Halo')
        self.check_for_row_in_list_table('2: Apa')
        self.check_for_row_in_list_table('3: Kabar')
        self.check_for_row_in_list_table('4: Pagi')
        self.check_for_row_in_list_table('5: Ini')
        status = self.browser.find_element_by_id('status')
        self.assertEqual(
                status.text,
                'oh tidak'
        )
        """

        
        # She types "Buy peacock feathers" into a text box (Edith's hobby
        # is tying fly-fishing lures)
        inputbox.send_keys('2147483647')

        
        
        # When she hits enter, she is taken to a new URL,
        # and now the page lists "1: Buy peacock feathers" as an item in a
        # to-do list table
        inputbox.send_keys(Keys.ENTER)
        edith_list_url = self.browser.current_url
        self.assertRegex(edith_list_url, '/lists/.+') #1
        self.check_for_row_in_list_table('1: 01111111111111111111111111111111')

       
